$(function() {

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    };

    var ua = window.navigator.userAgent;
    var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
    var webkit = !!ua.match(/WebKit/i);
    var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);

    svg4everybody();

    $( window ).on( "orientationchange", function( event ) {
        console.log( "This device is in " + event.orientation + " mode!" );

        if(($(window).width() < 961) && (event.orientation === 'landscape')) {
            $('body').addClass('turn');
        } else {
            $('body').removeClass('turn');
        }
    });

    function createFullpage() {
        $('#fullpage').fullpage({
            fixedElements: '#header',
            menu: '#menu',
            fitToSection: true,
            keyboardScrolling: true,
            animateAnchor: true,
            verticalCentered: false,
            // scrollOverflow: true,
            // scrollOverflowOptions: {
            //     click:false,
            //     tap: true
            // },
            resize: true,
            licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
            normalScrollElements: '.js-modal',
            onLeave: function(index, nextIndex, direction) {
                if (direction == 'down') {
                    $('.header').addClass('_bg');
                };

                if (index == 2 && direction == 'up') {
                    $('.header').removeClass('_bg');
                }
            }
        });
    }

    createFullpage();

    if ($('.heroes-slider').length) {
        var heroesSlider = new Swiper('.heroes-slider', {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            },
        });

        var bullets = $('.heroes-slider .swiper-pagination-bullet');
        $.each(bullets, function() {
            var order = $(this).index() + 1;
            $(this).text(order);
        })
    };

    if ($('.js-select-field').length) {
        $('.js-select-alt').fadeOut();
        $('.js-select-field').on('click', function() {
            $(this).closest('.js-select-wrapper').toggleClass('opened');
        });

        $('.js-dropdown-option').on('click', function() {
            var newVal = $(this).val();
            var newId = $(this).attr('id');
            $(this).closest('.js-select-wrapper').find('.js-select-field').val(newVal);
            $(this).closest('.js-select-wrapper').find('li').removeClass('selected');
            $(this).closest('li').addClass('selected');
            $(this).closest('.js-select-wrapper').toggleClass('opened');

            if (newId === 'other') {
                $(this).closest('.js-select-wrapper').find('.js-select-alt').fadeIn();
                $(this).closest('.js-select-wrapper').find('.js-select-alt .form__field').focus();
            } else {
                $(this).closest('.js-select-wrapper').find('.js-select-alt').fadeOut();
            }
        });
    };

    $(document).mouseup(function (e) {
        var select = $('.js-select-wrapper');
        if (select.has(e.target).length === 0) {
            $('.js-select-wrapper').removeClass('opened');
        }
    });

    $(this).keydown(function (eventObject) {
        if (eventObject.which == 27) {
            $('.js-select-wrapper').removeClass('opened');
        }
    });

    if ($('.js-file').length) {
        $('.js-file').on('change', function() {
            var imgPath = this.value.split('\\');
            $(this).closest('.js-upload-block').find('.js-upload-info span').text(imgPath[2]);
            console.log(imgPath[2])
        });
    };

    if ($('.scrollable').length) {
        $('.scrollable').mCustomScrollbar();
    };

    $('.js-menu-trigger').on('click', function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.js-menu').removeClass('opened');
            $.fn.fullpage.setAllowScrolling(true);
        } else {
            $(this).addClass('active');
            $('.js-menu').css('height', $('html').innerHeight()).addClass('opened');
            $.fn.fullpage.setAllowScrolling(false);
        }
        
    });

    if ($(window).width() < 1025) {
        $('.main-nav__item').on('click', function() {
            $('.js-menu-trigger').removeClass('active');
            $('.js-menu').removeClass('opened');
            $.fn.fullpage.setAllowScrolling(true);
        })
    }

    var adaptiveSlider = function() {
        var mobileSlider;

        if ($(window).width() < 1024) {
            $('.mobile-slider').addClass('swiper-container');
            $('.mobile-slider-wrapper').addClass('swiper-wrapper');
            $('.mobile-slide').addClass('swiper-slide');
            mobileSlider = new Swiper('.mobile-slider', {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }
            });
        } else {
            $('.mobile-slider').removeClass('swiper-container');
            $('.mobile-slider-wrapper').removeClass('swiper-wrapper');
            $('.mobile-slide').removeClass('swiper-slide');
            mobileSlider = new Swiper('.mobile-slider');
            mobileSlider.destroy();
            $('.mobile-slide').css('width', '33.333333%');
        }
    };

    var resultsCount = function() {
        if ($('.results-mobile').length) {
            if ($(window).width() < 1024) {
                $.each($('.results-mobile table'), function() {
                    if ($(this).index() > 1) {
                        $(this).hide();
                    }
                })
            };
            if ($(window).width() < 768) {
                $.each($('.results-mobile table'), function() {
                    if ($(this).index() > 0) {
                        $(this).hide();
                    }
                })
            }
        }
    };

    resultsCount();

    if ($('.mobile-slider').length) {
        adaptiveSlider();
    };

    $(window).on('resize', function() {
        adaptiveSlider();
        createFullpage();
        resultsCount();
        //$.fn.fullpage.reBuild();
    });


    // $('#menu a').on('click', function(e) {
    //     var $target = $($(this).attr('href')),
    //         targetOffset = $target.offset().top;

    //     if ($target.length) {
    //         e.preventDefault();

    //         $('html, body').animate({
    //             scrollTop: targetOffset
    //         }, 600);
    //     }
    // })

});


